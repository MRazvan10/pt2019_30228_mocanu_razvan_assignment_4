package Windows;

import java.sql.*;
import java.util.*;
import java.util.logging.*;
import javax.swing.*;
import javax.swing.table.*;

import Objects.ClientClass;

public class FirstWindow extends javax.swing.JFrame {

	private static final long serialVersionUID = 1L;

	public FirstWindow() {
		initComponents();
		Show_client_In_JTable();
	}

	int pos;

	public Connection getConnection() {
		Connection con = null;
		try {
			String url = "jdbc:mysql://localhost:3306/schooldb?serverTimezone=" + TimeZone.getDefault().getID();
			con = DriverManager.getConnection(url, "root", "dasdasdas2");
			return con;
		} catch (SQLException ex) {
			Logger.getLogger(FirstWindow.class.getName()).log(Level.SEVERE, null, ex);
			JOptionPane.showMessageDialog(null, "Not Connected");
			return null;
		}

	}

	public boolean checkInputs() {
		if (txt_NumeClient.getText() == null|| txt_Email.getText() == null|| txt_Telefon.getText() == null) {
			return false;
		} else {
			return true;
		}
	}



	public ArrayList<ClientClass> getProductList() {
		ArrayList<ClientClass> productList = new ArrayList<ClientClass>();
		Connection con = getConnection();
		String query = "SELECT * FROM client";

		Statement st;
		ResultSet rs;

		try {
			st = con.createStatement();
			rs = st.executeQuery(query);
			ClientClass client;

			while (rs.next()) {
				client = new ClientClass(rs.getInt("idClient"), rs.getString("NumeClient"),rs.getString("Email"), rs.getString("Telefon"));
				productList.add(client);
			}

		} catch (SQLException ex) {
			Logger.getLogger(FirstWindow.class.getName()).log(Level.SEVERE, null, ex);
		}
		return productList;
	}

	public void Show_client_In_JTable() {
		ArrayList<ClientClass> list = getProductList();
		DefaultTableModel model = (DefaultTableModel) JTable_client.getModel();

		model.setRowCount(0);
		Object[] row = new Object[4];
		for (int i = 0; i < list.size(); i++) {
			row[0] = list.get(i).getIdClient();
			row[1] = list.get(i).getNumeClient();
			row[2] = list.get(i).getEmail();
			row[3] = list.get(i).getTelefon();

			model.addRow(row);
		}
	}

	public void ShowItem(int index) {
		txt_idClient.setText(Integer.toString(getProductList().get(index).getIdClient()));
		txt_NumeClient.setText(getProductList().get(index).getNumeClient());
		txt_Email.setText(getProductList().get(index).getEmail());
		txt_Telefon.setText(getProductList().get(index).getTelefon());

	}

	private void initComponents() {

		jFrame1 = new javax.swing.JFrame();
		jFrame2 = new javax.swing.JFrame();
		jPanel1 = new javax.swing.JPanel();
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jLabel3 = new javax.swing.JLabel();
		jLabel4 = new javax.swing.JLabel();
		txt_NumeClient = new javax.swing.JTextField();
		txt_idClient = new javax.swing.JTextField();
		txt_Email = new javax.swing.JTextField();
		txt_Telefon = new javax.swing.JTextField();
		jScrollPane1 = new javax.swing.JScrollPane();
		JTable_client = new javax.swing.JTable();
		Btn_Insert = new javax.swing.JButton();
		Btn_Update = new javax.swing.JButton();
		Btn_Delete = new javax.swing.JButton();

		javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
		jFrame1.getContentPane().setLayout(jFrame1Layout);
		jFrame1Layout.setHorizontalGroup(
				jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 400, Short.MAX_VALUE)
				);
		jFrame1Layout.setVerticalGroup(
				jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 300, Short.MAX_VALUE)
				);


		javax.swing.GroupLayout jFrame2Layout = new javax.swing.GroupLayout(jFrame2.getContentPane());
		jFrame2.getContentPane().setLayout(jFrame2Layout);
		jFrame2Layout.setHorizontalGroup(
				jFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 400, Short.MAX_VALUE)
				);
		jFrame2Layout.setVerticalGroup(
				jFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 300, Short.MAX_VALUE)
				);

		//setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		jPanel1.setBackground(new java.awt.Color(255, 255, 204));

		jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18));
		jLabel1.setText("ID Client");

		jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18));
		jLabel2.setText("NumeClient");

		jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18));
		jLabel3.setText("Email");

		jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18));
		jLabel4.setText("Telefon");


		txt_NumeClient.setFont(new java.awt.Font("Tahoma", 1, 14));
		txt_NumeClient.setPreferredSize(new java.awt.Dimension(140, 40));

		txt_Email.setFont(new java.awt.Font("Tahoma", 1, 14));
		txt_Email.setPreferredSize(new java.awt.Dimension(140, 40));

		txt_Telefon.setFont(new java.awt.Font("Tahoma", 1, 14));
		txt_Telefon.setPreferredSize(new java.awt.Dimension(140, 40));

		txt_idClient.setFont(new java.awt.Font("Tahoma", 1, 14));
		txt_idClient.setEnabled(false);
		txt_idClient.setPreferredSize(new java.awt.Dimension(59, 40));

		JTable_client.setModel(new javax.swing.table.DefaultTableModel(
				new Object [][] {

				},
				new String [] {
						"idClient", "NumeClient","Email","Telefon"
				}
				));
		JTable_client.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				JTable_clientMouseClicked(evt);
			}
		});
		jScrollPane1.setViewportView(JTable_client);


		Btn_Insert.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
		Btn_Insert.setIcon(new javax.swing.ImageIcon(getClass().getResource("/project icon/Add_16x16.png"))); // NOI18N
		Btn_Insert.setText("Insert");
		Btn_Insert.setIconTextGap(15);
		Btn_Insert.setMaximumSize(new java.awt.Dimension(97, 33));
		Btn_Insert.setMinimumSize(new java.awt.Dimension(97, 33));
		Btn_Insert.setPreferredSize(new java.awt.Dimension(97, 33));
		Btn_Insert.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				Btn_InsertActionPerformed(evt);
			}
		});

		Btn_Update.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
		Btn_Update.setIcon(new javax.swing.ImageIcon(getClass().getResource("/project icon/Refresh_16x16.png")));
		Btn_Update.setText("Update");
		Btn_Update.setIconTextGap(15);
		Btn_Update.setMaximumSize(new java.awt.Dimension(97, 33));
		Btn_Update.setMinimumSize(new java.awt.Dimension(97, 33));
		Btn_Update.setPreferredSize(new java.awt.Dimension(97, 33));
		Btn_Update.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				Btn_UpdateActionPerformed(evt);
			}
		});



		Btn_Delete.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
		Btn_Delete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/project icon/Delete_16x16.png")));
		Btn_Delete.setText("Delete");
		Btn_Delete.setIconTextGap(15);
		Btn_Delete.setMaximumSize(new java.awt.Dimension(97, 33));
		Btn_Delete.setMinimumSize(new java.awt.Dimension(97, 33));
		Btn_Delete.setPreferredSize(new java.awt.Dimension(97, 33));
		Btn_Delete.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				Btn_DeleteActionPerformed(evt);
			}
		});



		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(
				jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
						.addGap(36, 36, 36)
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(jPanel1Layout.createSequentialGroup()
										.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
												.addComponent(jLabel1)
												.addComponent(jLabel2)
												.addComponent(jLabel3)
												.addComponent(jLabel4)
												)
										.addGroup(jPanel1Layout.createSequentialGroup()
												.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
																.addComponent(txt_NumeClient, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																.addComponent(txt_idClient, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																.addComponent(txt_Email, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																.addComponent(txt_Telefon, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)

																))
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addGap(50, 50, 50))
										.addGap(18, 18, 18)
										.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

												.addGroup(jPanel1Layout.createSequentialGroup()
														.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
																.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
																.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																		))
														.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
								.addGroup(jPanel1Layout.createSequentialGroup()
										.addComponent(Btn_Insert, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(5, 5, 5))))
				.addGroup(jPanel1Layout.createSequentialGroup()
						.addGap(160,160, 160)
						.addComponent(Btn_Update, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(Btn_Delete, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						)
				);
		jPanel1Layout.setVerticalGroup(
				jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
								.addGroup(jPanel1Layout.createSequentialGroup()
										.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel1)
												.addComponent(txt_idClient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(40, 40, 40)
										.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel2)
												.addComponent(txt_NumeClient, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addGap(68, 68, 68)
										.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel3)
												.addComponent(txt_Email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(40, 40, 40)
										.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel4)
												.addComponent(txt_Telefon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(40, 40, 40)
										)
								.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 326, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(50, 50, 50)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(Btn_Delete, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(Btn_Insert, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(Btn_Update, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
						.addContainerGap()
						)
				);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				);
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				);

		pack();
		setVisible(true);
	}

	private void Btn_InsertActionPerformed(java.awt.event.ActionEvent evt) {

		if (checkInputs()) {
			try {
				Connection con = getConnection();
				PreparedStatement ps = con.prepareStatement("INSERT INTO client (NumeClient,Email,Telefon) " + "values (?,?,?)");
				ps.setString(1, txt_NumeClient.getText());
				ps.setString(2, txt_Email.getText());
				ps.setString(3, txt_Telefon.getText());
				ps.executeUpdate();
				Show_client_In_JTable();

				JOptionPane.showMessageDialog(null, "Data Inserted");
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, ex.getMessage());
			}
		} else {
			JOptionPane.showMessageDialog(null, "One or More Field Are Empty");
		}
	}

	private void Btn_UpdateActionPerformed(java.awt.event.ActionEvent evt) {
		if (checkInputs() && txt_idClient.getText() != null) {
			String UpdateQuery = null;
			PreparedStatement ps = null;
			Connection con = getConnection();

			try {
				UpdateQuery = "UPDATE client SET NumeClient = ? , Email = ? , Telefon = ? WHERE idClient = ?";
				ps = con.prepareStatement(UpdateQuery);
				ps.setString(1, txt_NumeClient.getText());
				ps.setString(2, txt_Email.getText());
				ps.setString(3, txt_Telefon.getText());
				ps.setInt(4, Integer.parseInt(txt_idClient.getText()));
				ps.executeUpdate();
				Show_client_In_JTable();
				JOptionPane.showMessageDialog(null, "Product Updated");

			} catch (SQLException ex) {
				Logger.getLogger(FirstWindow.class.getName()).log(Level.SEVERE, null, ex);
			}

		} else {
			JOptionPane.showMessageDialog(null, "One or More Fields Are Empty or Wrong");
		}
	}

	private void Btn_DeleteActionPerformed(java.awt.event.ActionEvent evt) {
		if (!txt_idClient.getText().equals("")) {
			try {
				Connection con = getConnection();
				PreparedStatement ps = con.prepareStatement("DELETE FROM client WHERE idClient=?");
				ps.setInt(1, Integer.parseInt(txt_idClient.getText()));
				ps.executeUpdate();
				Show_client_In_JTable();
				JOptionPane.showMessageDialog(null, "Product Deleted");

			} catch (SQLException ex) {
				Logger.getLogger(FirstWindow.class.getName()).log(Level.SEVERE, null, ex);
				JOptionPane.showMessageDialog(null, "Product Not Deleted");
			}

		} else {
			JOptionPane.showMessageDialog(null, "Product Not Deleted : No idClient To Delete");
		}
	}

	private void JTable_clientMouseClicked(java.awt.event.MouseEvent evt) {
		int index = JTable_client.getSelectedRow();
		ShowItem(index);
	}

	private javax.swing.JButton Btn_Delete;
	private javax.swing.JButton Btn_Insert;
	private javax.swing.JButton Btn_Update;
	private javax.swing.JTable JTable_client;
	private javax.swing.JFrame jFrame1;
	private javax.swing.JFrame jFrame2;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTextField txt_idClient;
	private javax.swing.JTextField txt_NumeClient;
	private javax.swing.JTextField txt_Email;
	private javax.swing.JTextField txt_Telefon;

}
