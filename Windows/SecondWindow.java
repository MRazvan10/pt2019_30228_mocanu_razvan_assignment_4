package Windows;

import java.sql.*;
import java.util.*;
import java.util.logging.*;
import javax.swing.*;
import javax.swing.table.*;

import Objects.ProdusClass;

public class SecondWindow extends javax.swing.JFrame {

	private static final long serialVersionUID = 1L;

	public SecondWindow() {
		initComponents();
		Show_Produs_In_JTable();
	}

	int pos;

	public Connection getConnection() {
		Connection con = null;
		try {
			String url = "jdbc:mysql://localhost:3306/schooldb?serverTimezone=" + TimeZone.getDefault().getID();
			con = DriverManager.getConnection(url, "root", "dasdasdas2");
			return con;
		} catch (SQLException ex) {
			Logger.getLogger(SecondWindow.class.getName()).log(Level.SEVERE, null, ex);
			JOptionPane.showMessageDialog(null, "Not Connected");
			return null;
		}

	}

	public boolean checkInputs() {
		if (txt_NumeProdus.getText() == null|| txt_pret.getText() == null || txt_cantitate.getText() == null ) {
			return false;
		} else {
			try {
				Integer.parseInt(txt_pret.getText());
				Integer.parseInt(txt_cantitate.getText());
				return true;
			} catch (Exception ex) {
				return false;
			}
		}
	}



	public ArrayList<ProdusClass> getProductList() {
		ArrayList<ProdusClass> productList = new ArrayList<ProdusClass>();
		Connection con = getConnection();
		String query = "SELECT * FROM Produs";

		Statement st;
		ResultSet rs;

		try {
			st = con.createStatement();
			rs = st.executeQuery(query);
			ProdusClass Produs;

			while (rs.next()) {
				Produs = new ProdusClass(rs.getInt("idProdus"), rs.getString("NumeProdus"),Integer.parseInt(rs.getString("Pret")),Integer.parseInt(rs.getString("Cantitate")));
				productList.add(Produs);
			}

		} catch (SQLException ex) {
			Logger.getLogger(SecondWindow.class.getName()).log(Level.SEVERE, null, ex);
		}
		return productList;
	}

	public void Show_Produs_In_JTable() {
		ArrayList<ProdusClass> list = getProductList();
		DefaultTableModel model = (DefaultTableModel) JTable_Produs.getModel();

		model.setRowCount(0);
		Object[] row = new Object[4];
		for (int i = 0; i < list.size(); i++) {
			row[0] = list.get(i).getIdProdus();
			row[1] = list.get(i).getNumeProdus();
			row[2] = list.get(i).getPret();
			row[3] = list.get(i).getCantitate();
			model.addRow(row);
		}
	}

	public void ShowItem(int index) {
		txt_idProdus.setText(Integer.toString(getProductList().get(index).getIdProdus()));
		txt_NumeProdus.setText(getProductList().get(index).getNumeProdus());
		txt_pret.setText(Integer.toString(getProductList().get(index).getPret()));
		txt_cantitate.setText(Integer.toString(getProductList().get(index).getCantitate()));
	}

	private void initComponents() {

		jFrame1 = new javax.swing.JFrame();
		jFrame2 = new javax.swing.JFrame();
		jPanel1 = new javax.swing.JPanel();
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jLabel3 = new javax.swing.JLabel();
		jLabel4 = new javax.swing.JLabel();
		txt_NumeProdus = new javax.swing.JTextField();
		txt_idProdus = new javax.swing.JTextField();
		txt_pret = new javax.swing.JTextField();
		txt_cantitate = new javax.swing.JTextField();
		jScrollPane1 = new javax.swing.JScrollPane();
		JTable_Produs = new javax.swing.JTable();
		Btn_Insert = new javax.swing.JButton();
		Btn_Update = new javax.swing.JButton();
		Btn_Delete = new javax.swing.JButton();

		javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
		jFrame1.getContentPane().setLayout(jFrame1Layout);
		jFrame1Layout.setHorizontalGroup(
				jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 400, Short.MAX_VALUE)
				);
		jFrame1Layout.setVerticalGroup(
				jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 300, Short.MAX_VALUE)
				);


		javax.swing.GroupLayout jFrame2Layout = new javax.swing.GroupLayout(jFrame2.getContentPane());
		jFrame2.getContentPane().setLayout(jFrame2Layout);
		jFrame2Layout.setHorizontalGroup(
				jFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 400, Short.MAX_VALUE)
				);
		jFrame2Layout.setVerticalGroup(
				jFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 300, Short.MAX_VALUE)
				);

		//setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		jPanel1.setBackground(new java.awt.Color(255, 255, 204));

		jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18));
		jLabel1.setText("ID Produs");

		jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18));
		jLabel2.setText("NumeProdus");

		jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18));
		jLabel3.setText("Pret Produs");

		jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18));
		jLabel4.setText("Cantitate Produs");

		txt_NumeProdus.setFont(new java.awt.Font("Tahoma", 1, 14));
		txt_NumeProdus.setPreferredSize(new java.awt.Dimension(140, 40));

		txt_pret.setFont(new java.awt.Font("Tahoma", 1, 14));
		txt_pret.setPreferredSize(new java.awt.Dimension(140, 40));

		txt_cantitate.setFont(new java.awt.Font("Tahoma", 1, 14));
		txt_cantitate.setPreferredSize(new java.awt.Dimension(140, 40));

		txt_idProdus.setFont(new java.awt.Font("Tahoma", 1, 14));
		txt_idProdus.setEnabled(false);
		txt_idProdus.setPreferredSize(new java.awt.Dimension(59, 40));

		JTable_Produs.setModel(new javax.swing.table.DefaultTableModel(
				new Object [][] {

				},
				new String [] {
						"idProdus", "NumeProdus","PretProdus","Cantitate"
				}
				));
		JTable_Produs.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				JTable_ProdusMouseClicked(evt);
			}
		});
		jScrollPane1.setViewportView(JTable_Produs);


		Btn_Insert.setFont(new java.awt.Font("Tahoma", 1, 12));
		Btn_Insert.setIcon(new javax.swing.ImageIcon(getClass().getResource("/project icon/Add_16x16.png"))); 
		Btn_Insert.setText("Insert");
		Btn_Insert.setIconTextGap(15);
		Btn_Insert.setMaximumSize(new java.awt.Dimension(97, 33));
		Btn_Insert.setMinimumSize(new java.awt.Dimension(97, 33));
		Btn_Insert.setPreferredSize(new java.awt.Dimension(97, 33));
		Btn_Insert.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				Btn_InsertActionPerformed(evt);
			}
		});

		Btn_Update.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
		Btn_Update.setIcon(new javax.swing.ImageIcon(getClass().getResource("/project icon/Refresh_16x16.png"))); // NOI18N
		Btn_Update.setText("Update");
		Btn_Update.setIconTextGap(15);
		Btn_Update.setMaximumSize(new java.awt.Dimension(97, 33));
		Btn_Update.setMinimumSize(new java.awt.Dimension(97, 33));
		Btn_Update.setPreferredSize(new java.awt.Dimension(97, 33));
		Btn_Update.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				Btn_UpdateActionPerformed(evt);
			}
		});



		Btn_Delete.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
		Btn_Delete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/project icon/Delete_16x16.png"))); // NOI18N
		Btn_Delete.setText("Delete");
		Btn_Delete.setIconTextGap(15);
		Btn_Delete.setMaximumSize(new java.awt.Dimension(97, 33));
		Btn_Delete.setMinimumSize(new java.awt.Dimension(97, 33));
		Btn_Delete.setPreferredSize(new java.awt.Dimension(97, 33));
		Btn_Delete.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				Btn_DeleteActionPerformed(evt);
			}
		});



		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(
				jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
						.addGap(36, 36, 36)
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(jPanel1Layout.createSequentialGroup()
										.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
												.addComponent(jLabel1)
												.addComponent(jLabel2)
												.addComponent(jLabel3)
												.addComponent(jLabel4)
												)
										.addGroup(jPanel1Layout.createSequentialGroup()
												.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
																.addComponent(txt_NumeProdus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																.addComponent(txt_idProdus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																.addComponent(txt_pret, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																.addComponent(txt_cantitate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)

																))
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addGap(50, 50, 50))
										.addGap(18, 18, 18)
										.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)

												.addGroup(jPanel1Layout.createSequentialGroup()
														.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
																.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
																.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																		))
														.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
								.addGroup(jPanel1Layout.createSequentialGroup()
										.addComponent(Btn_Insert, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(5, 5, 5))))
				.addGroup(jPanel1Layout.createSequentialGroup()
						.addGap(160,160, 160)
						.addComponent(Btn_Update, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(Btn_Delete, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
						)
				);
		jPanel1Layout.setVerticalGroup(
				jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
								.addGroup(jPanel1Layout.createSequentialGroup()
										.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel1)
												.addComponent(txt_idProdus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(40, 40, 40)
										.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel2)
												.addComponent(txt_NumeProdus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addGap(68, 68, 68)
										.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel3)
												.addComponent(txt_pret, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(40, 40, 40)
										.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(jLabel4)
												.addComponent(txt_cantitate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(40, 40, 40)

										)
								.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 326, javax.swing.GroupLayout.PREFERRED_SIZE))
						.addGap(50, 50, 50)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(Btn_Delete, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(Btn_Insert, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(Btn_Update, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
						.addContainerGap()
						)
				);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				);
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				);

		pack();
		setVisible(true);
	}

	private void Btn_InsertActionPerformed(java.awt.event.ActionEvent evt) {

		if (checkInputs()) {
			try {
				Connection con = getConnection();
				PreparedStatement ps = con.prepareStatement("INSERT INTO produs (NumeProdus,Pret,Cantitate) " + "values (?,?,?)");
				ps.setString(1, txt_NumeProdus.getText());
				ps.setInt(2, Integer.parseInt(txt_pret.getText()));
				ps.setInt(3, Integer.parseInt(txt_cantitate.getText()));
				ps.executeUpdate();
				Show_Produs_In_JTable();

				JOptionPane.showMessageDialog(null, "Data Inserted");
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, ex.getMessage());
			}
		} else {
			JOptionPane.showMessageDialog(null, "One or More Field Are Empty");
		}
	}

	private void Btn_UpdateActionPerformed(java.awt.event.ActionEvent evt) {
		if (checkInputs() && txt_idProdus.getText() != null) {
			String UpdateQuery = null;
			PreparedStatement ps = null;
			Connection con = getConnection();

			try {
				UpdateQuery = "UPDATE Produs SET NumeProdus = ? ,Pret = ? , Cantitate = ? WHERE idProdus = ?";
				ps = con.prepareStatement(UpdateQuery);
				ps.setString(1, txt_NumeProdus.getText());
				ps.setInt(2, Integer.parseInt(txt_pret.getText()));
				ps.setInt(3, Integer.parseInt(txt_cantitate.getText()));
				ps.setInt(4, Integer.parseInt(txt_idProdus.getText()));
				ps.executeUpdate();
				Show_Produs_In_JTable();
				JOptionPane.showMessageDialog(null, "Product Updated");

			} catch (SQLException ex) {
				Logger.getLogger(SecondWindow.class.getName()).log(Level.SEVERE, null, ex);
			}

		} else {
			JOptionPane.showMessageDialog(null, "One or More Fields Are Empty or Wrong");
		}
	}

	private void Btn_DeleteActionPerformed(java.awt.event.ActionEvent evt) {
		if (!txt_idProdus.getText().equals("")) {
			try {
				Connection con = getConnection();
				PreparedStatement ps = con.prepareStatement("DELETE FROM Produs WHERE idProdus=?");
				ps.setInt(1, Integer.parseInt(txt_idProdus.getText()));
				ps.executeUpdate();
				Show_Produs_In_JTable();
				JOptionPane.showMessageDialog(null, "Product Deleted");

			} catch (SQLException ex) {
				Logger.getLogger(SecondWindow.class.getName()).log(Level.SEVERE, null, ex);
				JOptionPane.showMessageDialog(null, "Product Not Deleted");
			}

		} else {
			JOptionPane.showMessageDialog(null, "Product Not Deleted : No idProdus To Delete");
		}
	}

	private void JTable_ProdusMouseClicked(java.awt.event.MouseEvent evt) {
		int index = JTable_Produs.getSelectedRow();
		ShowItem(index);
	}


	private javax.swing.JButton Btn_Delete;
	private javax.swing.JButton Btn_Insert;
	private javax.swing.JButton Btn_Update;
	private javax.swing.JTable JTable_Produs;
	private javax.swing.JFrame jFrame1;
	private javax.swing.JFrame jFrame2;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTextField txt_idProdus;
	private javax.swing.JTextField txt_NumeProdus;
	private javax.swing.JTextField txt_pret;
	private javax.swing.JTextField txt_cantitate;

}
