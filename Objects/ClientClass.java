package Objects;

public class ClientClass {

	private int id;
	private String name;
	private String email;
	private String telefon;

	public ClientClass(int id, String name,String email,String telefon) {
		this.id = id;
		this.name = name;
		this.email=email;
		this.telefon=telefon;
	}

	public int getIdClient() {
		return id;
	}

	public String getNumeClient() {
		return name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getTelefon() {
		return telefon;
	}
}
