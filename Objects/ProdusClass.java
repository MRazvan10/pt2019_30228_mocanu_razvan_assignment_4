package Objects;

public class ProdusClass {

	private int id;
	private String name;
	private int pret;
	private int cantitate;
	
	public ProdusClass(int id, String name,int pret,int cantitate) {
		this.id = id;
		this.name = name;
		this.pret=pret;
		this.cantitate=cantitate;
	}

	public int getIdProdus() {
		return id;
	}

	public String getNumeProdus() {
		return name;
	}
	
	public int getPret() {
		return pret;
	}
	
	public int getCantitate() {
		return cantitate;
	}

	public void setCantitate(int i) {
		this.cantitate=i;
		
	}


}
