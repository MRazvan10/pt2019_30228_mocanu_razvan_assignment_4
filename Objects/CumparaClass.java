package Objects;

public class CumparaClass {

	private int id;
	private int idClient;
	private int idProdus;
	private int cantitate;
	private String adresa;
	private int cost;

	public CumparaClass(int id, int idClient,int idProdus,int cantitate,String adresa,int cost) {
		this.id = id;
		this.idClient=idClient;
		this.idProdus=idProdus;
		this.cantitate = cantitate;
		this.adresa=adresa;
		this.cost=cost;
	}

	public int getIdComanda() {
		return id;
	}
	
	public int getIdClient() {
		return idClient;
	}
	
	public int getIdProdus() {
		return idProdus;
	}

	public int getCantitate() {
		return cantitate;
	}
	
	public String getAdresa() {
		return adresa;
	}
	
	public int getCost() {
		return cost;
	}
	
	public void setCost(int cost) {
		this.cost=cost;
	}
}
