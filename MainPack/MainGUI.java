
package MainPack;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.*;
import javax.swing.*;

import Windows.*;


public class MainGUI implements ActionListener{

	public JFrame magazinDB;
	public JLabel Label;
	JButton btnClienti, btnProduse, btnCumpara;
	Container cPane;

	public MainGUI() {
	}

	public void createGUI() {
		magazinDB = new JFrame("Magazin");
		cPane = magazinDB.getContentPane();
		cPane.setLayout(new GridBagLayout());
		arrangeComponents();
		magazinDB.pack();
		magazinDB.setVisible(true);
	}

	public void arrangeComponents() {
		cPane.setBackground(Color.red);
		Label = new JLabel("Bine ati venit la magazin");
		Label.setFont(new java.awt.Font("Arial", Font.BOLD, 100));
		Label.setOpaque(true);
		Label.setBackground(Color.WHITE);
		Label.setForeground(Color.BLUE);

		btnClienti = new JButton("Clienti");
		btnProduse = new JButton("Produse");
		btnCumpara = new JButton("Cumpara");
		btnClienti.setFont(new java.awt.Font("Arial", Font.ITALIC,20));
		btnClienti.setOpaque(true);
		btnClienti.setPreferredSize(new Dimension(400,400));
		btnClienti.setBackground(Color.BLUE);
		btnClienti.setForeground(Color.WHITE);
		btnProduse.setFont(new java.awt.Font("Arial", Font.ITALIC,20));
		btnProduse.setOpaque(true);
		btnProduse.setPreferredSize(new Dimension(300,300));
		btnProduse.setBackground(Color.BLUE);
		btnProduse.setForeground(Color.WHITE);
		btnCumpara.setFont(new java.awt.Font("Arial", Font.ITALIC,20));
		btnCumpara.setOpaque(true);
		btnCumpara.setPreferredSize(new Dimension(400,400));
		btnCumpara.setBackground(Color.BLUE);
		btnCumpara.setForeground(Color.WHITE);
		GridBagConstraints gridBagConstraintsx01 = new GridBagConstraints();
		gridBagConstraintsx01.gridx = 5;
		gridBagConstraintsx01.gridy = 0;
		cPane.add(Label, gridBagConstraintsx01);



		GridBagConstraints gridBagConstraintsx09 = new GridBagConstraints();
		gridBagConstraintsx09.gridx = 0;
		gridBagConstraintsx09.gridy = 4;
		cPane.add(btnClienti, gridBagConstraintsx09);

		GridBagConstraints gridBagConstraintsx10 = new GridBagConstraints();
		gridBagConstraintsx10.gridx = 5;
		gridBagConstraintsx10.gridy = 4;
		cPane.add(btnProduse, gridBagConstraintsx10);

		GridBagConstraints gridBagConstraintsx11 = new GridBagConstraints();
		gridBagConstraintsx11.gridx = 10;
		gridBagConstraintsx11.gridy = 4;
		cPane.add(btnCumpara, gridBagConstraintsx11);



		btnClienti.addActionListener(this);
		btnProduse.addActionListener(this);
		btnCumpara.addActionListener(this);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnClienti) {
			java.awt.EventQueue.invokeLater(new Runnable() {
				@Override
				public void run() {
					new FirstWindow().setVisible(true);
				}
			});
		} 
		if (e.getSource() == btnProduse) {
			java.awt.EventQueue.invokeLater(new Runnable() {
				@Override
				public void run() {
					new SecondWindow().setVisible(true);
				}
			});
		} 
		if (e.getSource() == btnCumpara) {
			java.awt.EventQueue.invokeLater(new Runnable() {
				@Override
				public void run() {
					new ThirdWindow().setVisible(true);
				}
			});
		}

	}
}
